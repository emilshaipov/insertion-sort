package ru.ser.insertionSort;

import java.util.Arrays;

/**
 * @author Shaipov Emil" 18ИТ18
 *
 * Упорядочить числовую последовательность по возрастанию с использованием сортировки вставками.
 */

public class InsertionSort {
    public static void main(String[] args) {
        int[] array = {5, 2, 3, 1, 4, 8, 6, 9, 7};
        System.out.println("Массив который надо отсортировать: " + Arrays.toString(array));
        for (int left = 0; left < array.length; left++) {
            int value = array[left];
            int i = left - 1;
            for (; i >= 0; i--) {
                if (value < array[i]) {
                    array[i + 1] = array[i];
                } else {
                    break;
                }
            }
            array[i + 1] = value;
        }
        System.out.println("Отсортированный массив: " + Arrays.toString(array));
    }
}